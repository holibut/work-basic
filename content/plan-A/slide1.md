---
title: slide 2-1
weight: 10
---

# 七种武器(上) - 计划之道
A plan is typically any diagram or list of steps with timing and resources, used to achieve an objective. See also strategy. It is commonly understood as a temporal set of intended actions through which one expects to achieve a goal.
- Plans are of little importance, but planning is essential – Winston Churchill
- Plans are nothing; planning is everything. – Dwight D. Eisenhower
- A good plan, violently executed now, is better than a perfect plan next week. – George S. Patton
![](/images/plan/plan.jpg)
![](/images/plan/time-to-plan.jpg)

