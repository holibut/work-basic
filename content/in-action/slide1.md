---
title: slide 2-1
weight: 10
---

# 会休息的人更会工作
{{< blockquote author="Roman poet Ovid" >}}
  Take a rest. A field that has rested yields a beautiful crop.
{{< /blockquote >}}
