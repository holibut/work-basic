---
title: slide 2-1
weight: 10
---


# 工作的基本-PDCA

PDCA指 Plan, Do, Check和Act。 即计划／实施／检查和措施。

  它是([WIKIPEDIA](https://en.wikipedia.org/wiki/PDCA))：
               an iterative four-step management method used in business for the control and continuous improvement
                    of processes and products. 

又名：

plan–do–check–act 

plan–do–check–adjust

plan–do–study–act (PDSA)

