---
title: slide 2-2
weight: 10
---

# PDCA是持续改进

continually emphasized iterating towards an improved system, hence PDCA should be repeatedly implemented in spirals of increasing knowledge of the system that converge on the ultimate goal, each cycle closer than the previous. One can envision an open coil spring, with each loop being one cycle of the scientific method - PDCA, and each complete cycle indicating an increase in our knowledge of the system under study. This approach is based on the belief that our knowledge and skills are limited, but improving. Especially at the start of a project, key information may not be known; the PDCA—scientific method—provides feedback to justify our guesses (hypotheses) and increase our knowledge. Rather than enter "analysis paralysis" to get it perfect the first time, it is better to be approximately right than exactly wrong. With the improved knowledge, we may choose to refine or alter the goal (ideal state). Certainly, the PDCA approach can bring us closer to whatever goal we choose.
