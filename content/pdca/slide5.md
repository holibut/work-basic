---
title: slide 2-2
weight: 10
---

# Plan/Do/Check&amp;Study/Act
- Act - If the CHECK shows that the PLAN that was implemented in DO is an improvement to the prior standard (baseline), then that becomes the new standard (baseline) for how the organization should ACT going forward. If ... in DO is not an improvement, then the existing standard will remain ... In either case, if the CHECK showed something different than expected (whether better or worse), then there is some more learning to be done... and that will suggest potential future PDCA cycles. 
        