---
title: slide 2-2
weight: 10
---

#  PDCA是迭代

A fundamental principle of the scientific method and PDCA is iteration — once a hypothesis is confirmed (or negated), executing the cycle again will extend the knowledge further. Repeating the PDCA cycle can bring us closer to the goal, usually a perfect operation and output.

PDCA (and other forms of scientific problem solving) is also known as a system for developing critical thinking.
                