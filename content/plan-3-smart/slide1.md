---
title: slide 2-1
weight: 10
---


# 七种武器(上)-3. [SMART](https://en.wikipedia.org/wiki/SMART_criteria)
{{< rawhtml >}}
    <table class="ft-table">
                    <tr>
                        <th>Letter</th>
                        <th>Most common</th>
                        <th>Alternative</th>
                    </tr>
                    <tr>
                        <td>S</td>
                        <td>Specific</td>
                        <td>(Strategic and specific)</td>
                    </tr>
                    <tr>
                        <td>M</td>
                        <td>Measurable</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>A</td>
                        <td>Achievable</td>
                        <td>Agreed, attainable,action-oriented,ambitious, aligned with corporate goals,(agreed, attainable and achievable)</td>
                    </tr>
                    <tr>
                        <td>R</td>
                        <td>Relevant</td>
                        <td>Realistic, resourced, reasonable, (realistic and resourced), results-based</td>
                    </tr>
                    <tr>
                        <td>T</td>
                        <td>Time-bound</td>
                        <td>Time-based, time limited, time/cost limited, timely, time-sensitive, timeframe</td>
                    </tr>
                </table>
 {{< /rawhtml >}}

 <!-- ref: https://anaulin.org/blog/hugo-raw-html-shortcode/ -->