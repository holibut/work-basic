---
title: slide 2-1
weight: 10
---

# WBS - Design principles

{{< rawhtml >}}
                <ul>
                    <li>100% rule</li>
                    <li>Mutually exclusive elements</li>
                    <li>Plan outcomes, not actions</li>
                    <li>Level of detail</li>
                    <p>The lowest elements in a tree structure, a terminal element is one that is not further subdivided. In a Work Breakdown Structure such (activity or deliverable) elements are the items that are estimated in terms of resource requirements, budget and duration;</p>

                </ul>

{{< /rawhtml >}}