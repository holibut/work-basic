---
title: slide 2-1
weight: 10
---


# 七种武器(上)-4. [WBS](https://en.wikipedia.org/wiki/Work_breakdown_structure)

A work breakdown structure (WBS), in project management and systems engineering, is a deliverable-oriented decomposition of a project into smaller components.
{{< rawhtml >}}
                <p>WBS is a hierarchical and incremental decomposition of the project into phases, deliverables and work packages. It is a tree structure, which shows a subdivision of effort required to achieve an objective;</p>
                
{{< /rawhtml >}}