---
title: slide 2-1
weight: 10
---

# 七种武器之(下)-3.番茄(Pomodoro Technique)
Francesco Cirillo's "Pomodoro Technique" was originally conceived in the late 1980s and gradually refined until it was later defined in 1992. The technique is the namesake of a pomodoro (Italian for tomato) shaped kitchen timer initially used by Cirillo during his time at university. The "Pomodoro" is described as the fundamental metric of time within the technique and is traditionally defined as being 30 minutes long, consisting of 25 minutes of work and 5 minutes of break time. Cirillo also recommends a longer break of 15 to 30 minutes after every four Pomodoros. Through experimentation involving various work groups and mentoring activities, Cirillo determined the "ideal Pomodoro" to be 20–35 minutes long.
 
