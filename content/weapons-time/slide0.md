---
title: slide 2-1
weight: 10
---
# 七种武器(下)-时间管理
{{< blockquote author="William Shakespeare" >}}
  Better three hours too soon, than one minute too late.
{{< /blockquote >}}


{{< blockquote author="Stephen R. Covey" >}}
  he key is in not spending time, but in investing it.
{{< /blockquote >}}


{{< blockquote author="Carl Sandburg" >}}
  The time for action is now. It’s never too late to do something.
{{< /blockquote >}}

