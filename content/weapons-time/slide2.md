---
title: slide 2-1
weight: 10
---


 {{< rawhtml >}}
 <h1>七种武器之(下)-1.两仪四象　</h1>

                <p>
                    Using the Eisenhower Decision Principle, tasks are evaluated using the criteria important/unimportant and urgent/not urgent, and then placed in according quadrants in an Eisenhower Matrix (also known as an "Eisenhower Box" or "Eisenhower Decision Matrix").

                </p>
                <ul>
                    <li>1. I/U: crises, deadlines, problems.
                    </li>
                    <li>2. I/N-U: relationships, planning, recreation.</li>
                    <li> 3. U/U: interruptions, meetings, activities. </li>
                    <li>4. U/N-U: time wasters, pleasant activities, trivia.</li>
                </ul>
                <a href="https://en.wikipedia.org/wiki/Time_management
                         ">Eisenhower Method@WIKIPEDIA</a>
    
 {{< /rawhtml >}}