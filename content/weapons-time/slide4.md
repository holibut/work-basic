---
title: slide 2-1
weight: 10
---


# 七种武器之(下)-2.GTD
GTD(Getting Things Done) was created by David Allen and the basic idea behind this method is to finish all the small tasks immediately and a big task is to be divided into smaller tasks to start completing now. The reasoning behind this is to avoid the information overload or "brain freeze" which is likely to occur when there are hundreds of tasks. The thrust of GTD is to encourage the user to get their tasks and ideas out and on paper and organized as quickly as possible so they're easy to manage and see.

