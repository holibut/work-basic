---
title: slide 2-1
weight: 10
---


# 七种武器(上)-1. SWOT(SWOT matrix)
[SWOT](https://en.wikipedia.org/wiki/SWOT_analysis) analysis is an initialism for strengths, weaknesses, opportunities, and threats—and is a structured planning method that evaluates those four elements of a project or business venture. 
- Strengths: characteristics of the business or project that give it an advantage over others 
- Weaknesses: characteristics that place the business or project at a disadvantage relative to others 
- Opportunities: elements that the business or project could exploit to its advantage 
- Threats: elements in the environment that could cause trouble for the business or project

    