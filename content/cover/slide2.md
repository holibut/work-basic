---
title: slide 1-2
weight: 10
---


# Summary
                        
- 工作的基本-PDCA (What)
- PDCA的原理(Why)
- P的应用-七种武器之四(How)
- DCA的应用-七种武器之三(How)
- PDCA的实践-In action
